﻿$(document).ready(function() {

    const selectContact = function( element ) {
        $("aside ul li").removeClass('active');
        $(element).addClass('active');

        let dataObj = $(element).attr('data-details');
            dataObj = JSON.parse(dataObj || "{}");

        let dataUUID = dataObj.UUID;
        let dataAd = dataObj.Ad;
        let dataSoyad = dataObj.Soyad;
        let dataFirma = dataObj.Firma;
        let details = dataObj.ContactInformations || {};

        $('[name="UUID"]').val(dataUUID);
        $("#input_ad").val(dataAd);
        $("#input_soyad").val(dataSoyad);
        $("#input_firma").val(dataFirma);

        $("#title_name").text(`${dataAd} ${dataSoyad}`);
        $("#title_firma").text(dataFirma);

        $("#iletisim_satirlari").html('');
        let template_iletisim = $("#template_iletisim").html();
        for(let i=0; i < details.length; i++) {

            $("#iletisim_satirlari").append( template_iletisim );

            $("#iletisim_satirlari .iletisim_satir").eq(i).find('[name=info_type]').val( details[i].InfoType );
            $("#iletisim_satirlari .iletisim_satir").eq(i).find('[name=info_content]').val( details[i].InfoContent );

        }

    }

    $(document).on("click", "[data-contact]", function() {
        selectContact(this);
    });

    selectContact( $("aside ul li").eq(0) );

    $("#saveContact").submit(function(e) {

        e.preventDefault();

        const thisForm = $(this);

        $.ajax({
            url: "/Contacts/SavePerson",
            type: "post",
            data: $(thisForm).serialize(),
            beforeSend: function() {
                $(thisForm).css({opacity: "0.5"});
            },
            success: function(res) {
                swal("Başarılı", "Başarıyla kaydedildi", "success").then(() => window.location = '/');
            },
            error: function() {
                swal("Hata", "Kaydedilirken bir hata meydana geldi.", "error");
            },
            complete: function() {
                $(thisForm).css({opacity: "1"});
            }
        });

    });

    $(".add-new-row").click(function() {

        $("#iletisim_satirlari").append( $("#template_iletisim").html() );

        return false;
    });

    $(document).on('click', ".remove-row", function() {

        $(this).parents('.row')[0].remove();

        return false;
    });

    $("#yeniKisiEkle").click(function() {

        $("#saveContact input").val('');
        $('[name="UUID"]').val(0);
        $("#iletisim_satirlari").html('');
        $("#title_name").text('');
        $("#title_firma").text('');

        $(".add-new-row").trigger('click');

    });

    $("#input_ad, #input_soyad, #input_firma").on('keyup', function() {

        let Ad = $("#input_ad").val();
        let Soyad = $("#input_soyad").val();
        let Firma = $("#input_firma").val();

        $("#title_name").text(`${Ad} ${Soyad}`);
        $("#title_firma").text(Firma);

    });

    $(".deleteContact").click(function() {
        let thisButton = $(this);
        let UUID = $(this).attr('data-UUID');

        swal("Bu kaydı silmek üzresiniz, emin misiniz?", {
        buttons: {
            cancel: "Vazgeç",
            catch: {
                text: "Eminim, sil!",
                value: "deleteData",
            },
        },
        })
        .then((value) => {
            switch (value) {

                case "deleteData":

                $.ajax({
                    url: "/Contacts/RemovePerson",
                    type: "POST",
                    data: "UUID=" + UUID,
                    success: (res) => {
                        $(thisButton).parents('li')[0].remove();
                        swal("Başarılı", "Başarıyla silindi", "success");
                    },
                    error: (err) => {
                        swal("Hata", "Silinirken bir hata meydana geldi.", "error");
                    }
                });

                break;
            }
        });
    });

    $(".create-new-report-request").click(function() {

        let thisForm = $(this);

        $.ajax({
            url: "/Reports/Create",
            type:"post",
            data: $(thisForm).serialize(),
            beforeSend: function() {
                $(thisForm).prop('disabled', true);
            },
            success: function() {
                swal("Başarılı", "Rapor talebi başarı ile gönderildi.", "success").then(() => window.location = '/Reports');
            },
            error: function() {
                swal("Hata", "Rapor talebi sırasında bir hata oluştu.", "error");
            },
            complete: function() {
                $(thisForm).prop('disabled', false);
            }
        });

    });

});