using Microsoft.EntityFrameworkCore;
using TelefonRehberi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddControllersWithViews()
    .AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddDbContext<DataContext>(options =>
    options.UseNpgsql(connectionString));

builder.Services.AddScoped<Contact>();
builder.Services.AddScoped<ContactInformation>();
builder.Services.AddScoped<Report>();

builder.Services.AddScoped<ContactManage>();
builder.Services.AddScoped<ContactInformationManage>();
builder.Services.AddScoped<ReportManage>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseDeveloperExceptionPage();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=Index}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=SavePerson}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=RemovePerson}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=GetPersonDetails}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=AddInformationToPerson}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=GetStatistics}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=GetStatisticDetails}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Contacts}/{action=CreateStatistics}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Installation}/{action=index}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Reports}/{action=Index}"
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Reports}/{action=Create}"
);

app.Run();
