using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers;

public class ReportsController : Controller
{
    private readonly ILogger<ContactsController> _logger;

    private readonly DataContext _context;

    private readonly ContactManage _contactManage;

    private readonly ContactInformationManage _contactInformationManage;

    private readonly ReportManage _reportManage;

    public ReportsController(ILogger<ContactsController> logger, DataContext context, ContactManage contactManage, ContactInformationManage contactInformationManage, ReportManage reportManage)
    {
        _logger = logger;
        _context = context;
        _contactManage = contactManage;
        _contactInformationManage = contactInformationManage;
        _reportManage = reportManage;
    }

    public IActionResult Index()
    {
        var reports = _reportManage.GetReports();

        JsonConvert.SerializeObject(reports, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});

        return View(reports);
    }

    public IActionResult Create()
    {
        try {

            _reportManage.CreateReport();

            return Ok( "Rapor oluşturuldu" );

        } catch {
            return BadRequest("Oluşturma anında bir hata meydana geldi");
        }

    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
