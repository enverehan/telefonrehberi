using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers;

public class InstallationController : Controller
{
    private readonly ILogger<InstallationController> _logger;

    private readonly ContactManage _contactManage;

    private readonly ContactInformationManage _contactInformationManage;

    public InstallationController(ILogger<InstallationController> logger, ContactManage contactManage, ContactInformationManage contactInformationManage)
    {
        _logger = logger;
        _contactManage = contactManage;
        _contactInformationManage = contactInformationManage;
    }

    public IActionResult Index()
    {

        object[] Contacts = {
            new Contact {
                Ad = "Brad",
                Soyad = "Guerrero",
                Firma = "A Company"
            },
            new Contact {
                Ad = "Gruffydd",
                Soyad = "Macfarlane",
                Firma = "BBB"
            },
            new Contact {
                Ad = "Arley",
                Soyad = "Mcguire",
                Firma = "C Ltd. Şti"
            },
            new Contact {
                Ad = "Madina",
                Soyad = "Whitworth",
                Firma = "A4-Tech"
            },
            new Contact {
                Ad = "Isaac",
                Soyad = "Stephens",
                Firma = "Dell"
            },
            new Contact {
                Ad = "Gwion",
                Soyad = "Clarke",
                Firma = "Asus"
            },
            new Contact {
                Ad = "Bianca",
                Soyad = "Carrillo",
                Firma = "Apple"
            },
            new Contact {
                Ad = "Haydn",
                Soyad = "Connor",
                Firma = "Microsoft"
            },
            new Contact {
                Ad = "Federico",
                Soyad = "Stacey",
                Firma = "Lenovo"
            },
            new Contact {
                Ad = "Gwen",
                Soyad = "Barnard",
                Firma = "IBM"
            },
        };

        object[] CityPool = {
            "Adana", 
            "Ankara", 
            "Antalya", 
            "Aydın", 
            "Bursa", 
            "Çanakkale", 
            "Diyarbakır", 
            "Edirne", 
            "Eskişehir", 
            "Gaziantep", 
            "Hatay", 
            "Mersin", 
            "İstanbul", 
            "İzmir"
        };

        try {

            foreach(Contact contact in Contacts) {
                int UUID = _contactManage.AddContact(contact);
            }

            ViewData["Title"] = "Kurulum";
            ViewBag.InsertedCount = Contacts.Length;

            return View();

        } catch(Exception e) {

            throw new ArgumentException("Kurulum aşamasında bir hata meydana geldi. \n\n" + e);

        }
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
