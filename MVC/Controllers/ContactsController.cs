using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers;

public class ContactsController : Controller
{
    private readonly ILogger<ContactsController> _logger;

    private readonly DataContext _context;

    private readonly ContactManage _contactManage;

    private readonly ContactInformationManage _contactInformationManage;

    public ContactsController(ILogger<ContactsController> logger, DataContext context, ContactManage contactManage, ContactInformationManage contactInformationManage)
    {
        _logger = logger;
        _context = context;
        _contactManage = contactManage;
        _contactInformationManage = contactInformationManage;
    }

    public IActionResult Index()
    {
        var contacts = _contactManage.GetContacts();

        for(int i=0; i < contacts.Count(); i++) {
            contacts[i].ContactInformations = _context.ContactInformations.Where(b => b.ContactsUUID == contacts[i].UUID).ToArray();
        }

        return View(contacts);
    }

    public IActionResult GetPersonList()
    {
        return Ok("Get PersonList");
    }

    [HttpPost]
    public IActionResult SavePerson()
    {

        int UUID = int.Parse( Request.Form["UUID"] );
        string InputAd = Request.Form["input_ad"];
        string InputSoyad = Request.Form["input_soyad"];
        string InputFirma = Request.Form["input_firma"];

        string[] InfoType = Request.Form["info_type"];
        string[] InfoContent = Request.Form["info_content"];

        if( UUID == 0 ) {

            try {
                int NewID = _contactManage.AddContact(new Contact {
                    Ad = InputAd,
                    Soyad = InputSoyad,
                    Firma = InputFirma
                });

                for(int i=0; i < InfoType.Count(); i++) {

                    _contactInformationManage.AddInformationToContact(new ContactInformation {
                        InfoType = InfoType[i],
                        InfoContent = InfoContent[i],
                        ContactsUUID = NewID
                    });

                }

                var contacts = _contactManage.GetContacts().Where(d => d.UUID == NewID).ToArray();
                for(int i=0; i < contacts.Count(); i++) {
                    contacts[i].ContactInformations = _context.ContactInformations.Where(b => b.ContactsUUID == contacts[i].UUID).ToArray();
                }

                string returnValue = JsonConvert.SerializeObject(contacts, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});

                return Ok(returnValue);

            } catch {
                throw new ArgumentException("Eklenirken bir hata oluştu.");
            }

        } else {

            try {

                _contactManage.UpdateContact(new Contact {
                    UUID = UUID,
                    Ad = InputAd,
                    Soyad = InputSoyad,
                    Firma = InputFirma
                });

                _contactInformationManage.RemoveInformationContact(UUID);

                for(int i=0; i < InfoType.Count(); i++) {

                    _contactInformationManage.AddInformationToContact(new ContactInformation{
                        InfoType = InfoType[i],
                        InfoContent = InfoContent[i],
                        ContactsUUID = UUID
                    });

                }

                var contacts = _contactManage.GetContacts().Where(d => d.UUID == UUID).ToArray();
                for(int i=0; i < contacts.Count(); i++) {
                    contacts[i].ContactInformations = _context.ContactInformations.Where(b => b.ContactsUUID == contacts[i].UUID).ToArray();
                }

                string returnValue = JsonConvert.SerializeObject(contacts, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});

                return Ok(returnValue);
            } catch(Exception e) {
                throw new ArgumentException("Güncellenirken bir hata oluştu." + e);
            }

        }

    }

    [HttpPost]
    public IActionResult RemovePerson()
    {
        try {

            int UUID = int.Parse( Request.Form["UUID"] );

            _contactManage.RemoveContact(UUID);

            return Ok("Başarılı");
        } catch(Exception e) {
            throw new ArgumentException("Silinirken bir hata oluştu." + e);
        }

    }

    public IActionResult GetPersonDetails()
    {
        return Ok("GetPersonDetails");
    }

    public IActionResult AddInformationToPerson()
    {
        return Ok("AddInformationToPerson");
    }

    public IActionResult RemoveInformationFromPerson()
    {
        return Ok("RemoveInformationFromPerson");
    }

    public IActionResult GetStatistics()
    {
        return Ok("GetStatistics");
    }

    public IActionResult GetStatisticDetails()
    {
        return Ok("GetStatisticDetails");
    }

    public IActionResult CreateStatistics()
    {
        return Ok("CreateStatistics");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
