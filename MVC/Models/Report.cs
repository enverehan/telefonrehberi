using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace TelefonRehberi.Models;

public class Report
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int UUID { get; set; }

    [Required]
    public DateTime ReportDate { get; set; }

    [Required]
    [Column(TypeName = "text")]
    public string? ReportDetail { get; set; }

}

public class ReportManage {

    private readonly DataContext _context;

    public ReportManage(DataContext context)
    {
        _context = context;
    }

    public List<Report> GetReports()
    {
        return _context.Reports.ToList();
    }

    public void CreateReport()
    {
        var currentReport = from p in _context.ContactInformations
                                where p.InfoType == "Konum"
                                group p by p.InfoContent into g
                                select new
                                {
                                    InfoContent = g.Key,
                                    count = g.Count()
                                };

        string reportString = JsonConvert.SerializeObject(currentReport, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});

        _context.Reports.Add(new Report {
            ReportDate = DateTime.UtcNow,
            ReportDetail = reportString
        });
        _context.SaveChanges();
    }

    public void RemoveReport(Report Report)
    {
        _context.Reports.Remove(Report);
    }
}