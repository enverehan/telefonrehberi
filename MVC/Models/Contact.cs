using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelefonRehberi.Models {
    public class Contact
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UUID { get; set; }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string? Ad { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string? Soyad { get; set; }

        [Column(TypeName = "varchar(200)")]
        public string? Firma { get; set; }

        public ICollection <ContactInformation> ContactInformations { get; set; }

    }

    public class ContactManage
    {

        private readonly DataContext _context;

        public ContactManage(DataContext context)
        {
            _context = context;
        }

        public List<Contact> GetContacts()
        {
            return _context.Contacts.ToList();
        }

        public int AddContact(Contact contact)
        {
            _context.Contacts.Add(contact);
            _context.SaveChanges();

            return contact.UUID;
        }

        public void RemoveContact(int ID)
        {
            _context.Contacts.RemoveRange( _context.Contacts.Where(data => data.UUID == ID) );
            _context.SaveChanges();
        }

        public Contact UpdateContact(Contact contact)
        {
            var result = _context.Contacts.Where(d => d.UUID == contact.UUID).First<Contact>();
            result.Ad = contact.Ad;
            result.Soyad = contact.Soyad;
            result.Firma = contact.Firma;

            _context.SaveChanges();

            return contact;
        }
    }
}