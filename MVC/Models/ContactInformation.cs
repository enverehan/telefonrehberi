using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelefonRehberi.Models;

public class ContactInformation
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int UUID { get; set; }

    [Required]
    [Column(TypeName = "varchar(50)")]
    public string? InfoType { get; set; }

    [Required]
    [Column(TypeName = "varchar(200)")]
    public string? InfoContent { get; set; }

    public int ContactsUUID { get; set; }

    public Contact? Contacts { get; set; }

}

public class ContactInformationManage {

    private readonly DataContext _context;

    public ContactInformationManage(DataContext context)
    {
        _context = context;
    }

    public object GetInformationFromContacts(int UUID)
    {
        return _context.ContactInformations.ToList();
    }

    public object AddInformationToContact(ContactInformation ContactInformation)
    {
        _context.ContactInformations.Add(ContactInformation);
        _context.SaveChanges();

        return ContactInformation;
    }

    public void RemoveInformationContact(int ID)
    {
        _context.ContactInformations.RemoveRange( _context.ContactInformations.Where(data => data.ContactsUUID == ID) );
        _context.SaveChanges();
    }

}