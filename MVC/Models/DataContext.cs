using Microsoft.EntityFrameworkCore;

namespace TelefonRehberi.Models;

public class DataContext : DbContext
{
    private readonly IConfiguration Configuration;
    public DbSet<Contact> Contacts { get; set; }
    public DbSet<ContactInformation> ContactInformations { get; set; }
    public DbSet<Report> Reports { get; set; }

    public DataContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql(Configuration["ConnectionStrings:DefaultConnection"]);
}